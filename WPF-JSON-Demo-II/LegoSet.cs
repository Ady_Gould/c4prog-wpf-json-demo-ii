﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_JSON_Demo_II
{
    class LegoSet
    {
        // define the properties

        private String title;
        private String setNumber;
        private String series;
        private String picture;
        private Boolean own;
        private Boolean want;
        private int quantity;

        public LegoSet()
        {
            // empty constructor
        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public String Series
        {
            get { return series; }
            set { series = value; }
        }

        public String SetNumber
        {
            get { return setNumber; }
            set { setNumber = value; }
        }

        public String Picture
        {
            get { return picture; }
            set { picture = value; }
        }

        public int Quantity
        {
            get { return quantity; }
            set {
                if (value >= 0)
                {
                    quantity = value;
                }
                else
                {
                    quantity = 0;
                } // end if over 0
            } // end set quantity
        } // end get/set Quantity

        public Boolean Owned
        {
            get { return own; }
            set { own = value; }
        }

        public bool IsOwned()
        {
            return Owned;
        }

        public Boolean Wanted
        {
            get { return want; }
            set {want = value; }
        }

        public bool IsWanted()
        {
            return Wanted;
        }
        

    } // end class LegoSet

} // end namespace WPF JSON DEMO II
