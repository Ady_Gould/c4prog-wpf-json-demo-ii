﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//
using Newtonsoft.Json;
using System.IO;

namespace WPF_JSON_Demo_II
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Properties
        private LegoSet mySet;
        private List<LegoSet> legoSets = new List<LegoSet>();

        public MainWindow()
        {
            InitializeComponent();

            InitialiseSeriesComboBox();
        }

        private void InitialiseSeriesComboBox()
        {
            cboSeries.Items.Add("Duplo");
            cboSeries.Items.Add("Star Wars");
            cboSeries.Items.Add("Creator");
            cboSeries.Items.Add("Ninjago");
            cboSeries.Items.Add("Friends");
            cboSeries.Items.Add("Harry Potter");
            cboSeries.Items.Add("City");
            cboSeries.Items.Add("Technic");
        }

        private void btnSaveSet_Click(object sender, RoutedEventArgs e)
        {
            mySet = new LegoSet();
            mySet.Title = tbSetName.Text;
            mySet.SetNumber = tbSetNumber.Text;
            mySet.Picture = tbSetName.Text;
            mySet.Series = cboSeries.Items[cboSeries.SelectedIndex].ToString();
            //             cboSeries.SelectedItem.ToString();
            mySet.Owned = Convert.ToBoolean(chkOwned.IsChecked);
            mySet.Wanted = Convert.ToBoolean(chkWanted.IsChecked);

            mySet.Quantity = Convert.ToInt32(tbQuantity.Text);

            legoSets.Add(mySet);

            UpdateSetsListView();
        }

        private void UpdateSetsListView()
        {
            // Add columns
            var gridView = new GridView();
            lvSets.View = gridView;

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Title",
                DisplayMemberBinding = new Binding("Title")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Set No.",
                DisplayMemberBinding = new Binding("SetNumber")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Series Theme",
                DisplayMemberBinding = new Binding("Series")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Picture",
                DisplayMemberBinding = new Binding("Picture")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Want",
                DisplayMemberBinding = new Binding("IsWanted")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Own",
                DisplayMemberBinding = new Binding("IsOwned")
            });

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Quantity",
                DisplayMemberBinding = new Binding("Quantity")
            });

            lvSets.ItemsSource = legoSets;


        }

        private void btnSerializeSave_Click(object sender, RoutedEventArgs e)
        {
            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(@"lego-sets.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, legoSets);
            } // end using Stream writer
        }

        private void brnReadDeserialize_Click(object sender, RoutedEventArgs e)
        {
            using (StreamReader file = File.OpenText(@"lego-sets.json"))
            {
                String jsonText = File.ReadAllText(@"lego-sets.json");
                legoSets = JsonConvert.DeserializeObject<List<LegoSet>>(jsonText);
            } // end stream read

            UpdateSetsListView();
        }
    }
}
